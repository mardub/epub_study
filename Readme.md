## Intro
This repository contains a python and a bash script that allows you to do quick analysis of epub and/or txt files that contain a corpus in Swedish.
The bash script is optional.
Example of output (*.data.txt):
```
Number of sentences: 6527
Number of words: 69842
Number of words per sentences: 10.700474950206834
Top 20 most common adjectives: 
[('bra', 101), ('själv', 69), ('andra', 58), ('hela', 50), ('nya', 36), ('olika', 35), ('flera', 34), ('skönt', 32), ('många', 30), ('små', 29), ('ensam', 27), ('annat', 26), ('annan', 25), ('nöjd', 25), ('stor', 24), ('svårt', 23), ('liten', 22), ('nästa', 21), ('klart', 21), ('gamla', 21)]
Top 20 most common nouns: 
[('fall', 74), ('dag', 68), ('gården', 68), ('huset', 65), ('bilen', 64), ('skogen', 54), ('gästhuset', 53), ('köket', 48), ('par', 42), ('mobilen', 41), ('mat', 41), ('kaffe', 40), ('sjön', 40), ('kroppen', 39), ('bil', 39), ('dörren', 38), ('del', 38), ('Kapitel', 37), ('morgon', 36), ('hus', 35)]
Top 20 most common verbs: 
[('sa', 770), ('gick', 244), ('hade', 169), ('kom', 137), ('såg', 136), ('tog', 135), ('ha', 119), ('var', 108), ('tittade', 106), ('gå', 106), ('få', 91), ('bli', 90), ('ta', 89), ('fick', 89), ('måla', 85), ('se', 83), ('stod', 83), ('tänkte', 81), ('göra', 81), ('kände', 80)]
Top 20 most common averbes: 
[('i', 1225), ('på', 1187), ('med', 686), ('till', 612), ('för', 433), ('av', 417), ('om', 228), ('mot', 175), ('från', 140), ('efter', 116), ('vid', 107), ('över', 92), ('I', 60), ('åt', 47), ('under', 43), ('utan', 40), ('ur', 40), ('som', 39), ('genom', 38), ('På', 37)]
Top 20 most common proper nouns: 
[('Disa', 745), ('Petra', 230), ('Anton', 198), ('Bertil', 146), ('Silvia', 105), ('Ove', 104), ('Åsa', 102), ('Anna', 96), ('Inger', 71), ('Svalarp', 64), ('Stockholm', 35), ('Antons', 28), ('Günther', 27), ('Karl', 22), ('Nicklas', 22), ('Boxholm', 21), ('Linköping', 17), ('Svalarps', 17), ('Tranås', 16), ('Petras', 15)]
```
The scripts gives also your text lemmatised (*.lemma.txt) and removed from stopwrds (*.stop.txt) in order to be ready for treatments for Antconc.
## Prerequisites with Virtual env
If you use virtual env you can directly run these commands:
```bash
cd path/to/ebook_study
python3 -m venv ebook_env
#python -m venv ebook_env #try this command in case the previous one fails
source ebook_env/bin/activate
pip install -r requirements.txt
#then only if your files are in *.epub format run:
sh basic.sh 
```

## Extra
To use another corpus, just replace the ebooks in the folder "corpus"

# Description
This project aims at analysing Epub in swedish.

# Usage
If you already have a text in the txt format you can use directly the python script. For instance if your corpus is called "example.txt":
```
python stats.py example.txt
```
Make sure your file example.txt is in Swedish, is in your current directory and that you have installed the prerequisites above.

As an output you will get
- outputs (the folder in which you will find the files below)
- example.lemma.txt (your text lemmatized)
- example.stop.txt (your text without stopwords)
- example.lemma.stop.txt (your text both lemmatized and without stopwords)
- example.data.txt (some quick stats about your text)
- example.xlsx (Name entity detections)

# Context
 This collaboration between a Unix NLP developper and a Windows Litterary user is an opportunity to touch on typical necessary taks for collaboration of this type (conversion csv to xlsx, make lemmatized and stopword corpus, remove copyrights).

# Authors:
Original project of [AnnaCarin Billing](https://www.katalog.uu.se/empinfo/?id=N96-2024). Pilot project at CDHU
Engineer:
Marie Dubremetz
Gitlab:
[@mardub](https://gitlab.com/mardub)
Github:
[@mardub1635](https://github.com/mardub1635)
Website:
[http://www.uppsala.ai](http://www.uppsala.ai)
e-mail:
mardubr-github@yahoo.com

